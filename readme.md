# Advanced Programming Techniques (COSC 1076)

## Vending Machine / Assignment 2

### Description
The purpose of this assignment is to familiarise students to C Programming Language. The assignment is to make a game named Car Board. The aim of the game is to move the player around the board without hitting any obstacles. The complete requirements and description are available on the Git repo with file name `Assignment One (v1.0).pdf`. All of the source code here are debugged using Xcode on Mac. To find all the source codes, go to `./Car Board/` directory.

### Compiling the Source Code
In order to comply with the assignment specification, we use ANSI C. We need to give parameters before compiling the C source code. In this case, the source code has been tested on RMIT Jupiter, Saturn, and Titan Server running Linux (RedHat) and a Mac running MacOS 10.13.4. The GCC Compiler on RMIT Servers are version 4.8.5 (`gcc (GCC) 4.8.5 20150623 (Red Hat 4.8.5-16)`) and for my Mac is LLVM version 9.1.0 (`Apple LLVM version 9.1.0 (clang-902.0.39.1)`).

The command to compile the source code according to the assignment specification is as below:

`gcc -ansi -pedantic -Wall -o vm *.c`

The `-ansi` is required to compile using ANSI C standard (C 1989), `-pedantic` is used to warn the user if one of their codes are not safe for POSIX, and `-Wall` is used to warn the user if there is unsafe operation on the code. `-o` is used to specify output name and `*.c` to include all `.c` files (C Source Codes) to be compiled.

To simplify the compiling command, I have made a `makefile` that will automatically do this job for you. Simply execute `make` to compile the source code. If you want to delete the compiled source code (The Binary File), you can use predefined function on the makefile by executing `make clean` to remove the binary file instead of having executing `rm` command.

### How to run
To run the game, you need to compile the source code first using `make` or execute `gcc` command. See Compiling the Source Code section.

After finish compiling, open the binary file by executing the command `./vm ./stock.dat ./coins.dat`. The instruction will pop up and follow the instructions.

The program requires to receive 2 parameters.

`vm <stock file> <coin file>`

The above instruction already give the stock and coin file to the parameter.

### Bonus Marks

Based on my own testing, this source code complies with the bonus, requirement from the assignment specification, which is requirement 17, 18, and 19.
For requirement 17 (Implement Main Menu Using Function Pointers) can be found on `vm.c` line 16 and 28, and the whole `vm_menu.c` file.
For requirement 18 (Support Coins in the System) can be found on the whole `vm_coin.c` and several codes on `vm_options.c` that is related to coin system.
For requirement 19 (Validating the Input Files) can be found on the `loadStock()` and `loadCoins()` function. The validation system that I made is scan and validate (Validate on the go) per line.

### Changes

#### Version 1.0
* All Requirement's has been implemented successfuly.

#### Version 0.1 (Release for Demo Week 10)
* Initial release for Demo Week 10 on the Lab. Only Requirement 1 - 5 are finished.
