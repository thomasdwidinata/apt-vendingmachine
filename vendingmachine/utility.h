/******************************************************************************
** Student name: 	Thomas Dwi Dinata
** Student number: 	s3702763
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#ifndef UTILITY_H
#define UTILITY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <assert.h>

typedef enum boolean
{
    FALSE = 0,
    TRUE
} Boolean;

/**
 * Public variables
 * Declared by Thomas Dwi Dinata
 * Non-zero values are defined here to prevent magic values
 * and confusion on the program. It is defined on pre-processor
 * in case there are something wrong with the public values,
 * so the developer can easily maintain the program.
 */
#define NEW_LINE_SPACE 1
#define NULL_SPACE 1
#define NUMBER_BASE 10
#define FUNCTION_RETURN_ERROR -1
#define ID_OFFSET_LESS -1
#define ID_OFFSET_MORE 1
#define LESS_THAN_ONE 1
#define NEGATIVE_NEGATION -1
#define INDEX_OFFSET_DECREMENT_ONE 1
#define MENU_SAVE_EXIT 3
#define MENU_FORCE_EXIT 9
#define MENU_START_INDEX 1
#define ID_START_NUMBER 1

/**
 * Public Buffer Size Definition
 * Declared by : Thomas Dwi Dinata
 * Public buffer size, used for defining maximum buffer bytes size.
 * It might be used for scanning the input and File Read.
 **/
#define BUFFER_SIZE 256
#define LARGE_BUFFER 10000000

/**
 * FILE* Operations Definition
 * Declared by : Thomas Dwi Dinata
 * This definition is used for fopen() function.
 * The purpose of defining this is to make readable
 * source code.
 */
#define READ "r"
#define READ_SPECIAL "r+"
#define WRITE "w"
#define WRITE_SPECIAL "w+"
#define APPEND "a"
#define APPEND_SPECIAL "a+"

/**
 * Localisation Definition
 * Declared by : Thomas Dwi Dinata
 * Below are the literal string for the printf function
 * to show the graphical user interface.
 * Definition starts with "UI_" are for the User Interface.
 * Definition starts with "MENU_" are for the Menu Display.
 * Definition starts with "SYSTEM_" are for critical error
 * message or information.
 * Definition starts with "PROTOTYPE_" are for printf modifier
 **/
#define SYSTEM_MALLOC_FAIL "[  !  ] Unable to allocate memory for Vending Machine Application! Make sure you have enough memory!"
#define SYSTEM_FILE_ERROR "Unable to open/write the file. Make sure you have access to it. The data will be printed into the screen so you can copy and paste into the file. The file name that we are using is %s\n"
#define SYSTEM_DEBUG "[  !  ] "
#define SYSTEM_COIN_DISPENSE_ERROR "[  x  ]"
#define SYSTEM_COIN_DISPENSE_NO_CHANGES "[  v  ]"
#define SYSTEM_FILE_LINE_CORRUPT "The current file seems to be corrupted. The current line that is being read is '%s'. Skipping this line."
#define SYSTEM_INIT_FAILED "System initialisation failed! Please make sure that the database file is available!"
#define SYSTEM_SAVE_SUCCESS "Data saved successfuly."
#define SYSTEM_SAVE_FAILURE "Something happened during saving."
#define SYSTEM_SAVING "Saving database..."
#define UI_WELCOME NEWLINE "Welcome!" NEWLINE "Main Menu:" NEWLINE
#define UI_ADMIN_ONLY "Administrator-Only Menu:"
#define UI_USAGE "usage: vm <stockpath> <coinpath>\n\n\tstockpath\tThe path to Stock File\n\tcoinpath\tThe path to Coin Save File, which saves how many coins has been received by the machine\n"
#define PROTOTYPE_MAINMENU "%d.%s"
#define PROTOTYPE_PRICE "$%u.%02u"
#define PROTOTYPE_SCANF_BUFFER_FIX_1 "%*[^\n]"
#define PROTOTYPE_SCANF_BUFFER_FIX_2 "%*c"
#define PROTOTYPE_STOCK_FILE "%s|%s|%s|%u,%02u|%u"
#define PROTOTYPE_COIN_FILE "%d,%d"
#define PROTOTYPE_ID_GET "%[^0-9]%d"
#define PROTOTYPE_ID_NEW "I%04d"
#define PROTOTYPE_FIRST_ID "I0001"
#define PROTOTYPE_STOCK_PRINT_HEADER1 "%-5s | %-18s | %-9s | %-5s"
#define PROTOTYPE_STOCK_PRINT_HEADER2 "----------------------------------------------"
#define PROTOTYPE_STOCK_PRINT_ITEMS "%-5s | %-18s | %-9d | " PROTOTYPE_PRICE
#define PROTOTYPE_COIN_SUMMARY_TABLE_HEADER "%-13s | %s"
#define PROTOTYPE_COIN_SUMMARY_TABLE_CONTENT "%-13s | %u"
#define PROTOTYPE_UNDERLINE_START "%c[4m"
#define PROTOTYPE_UNDERLINE_STOP "%c[0m"
#define UNDERLINE_PARAMETER 27
#define UI_TABLE_HEADER "-"
#define UI_SELECT_MENU "Select your option (1-9): "
#define UI_INVALID_CHOICE "Invalid value! Select value from the instruction above."
#define UI_EXIT "Goodbye~"
#define UI_PURCHASE_ITEM_HEADER "Purchase Item" NEWLINE "-------------"
#define UI_NO_ITEM "There are no items in this vending machine. This operation cannot be executed."
#define UI_NO_ITEM_WARNING "There are no items in this vending machine."
#define UI_CANCEL_OPERATION "Operation canceled. Going back to main menu..."
#define UI_DENOM_ERROR "Error: %s is not a valid denomination of money! Accepted input are as example: 0.20; 0.50; 1.00; 2.00; 5.00; 10.00"
#define UI_PAYMENT_INCOMPLETE "You still need to give us " PROTOTYPE_PRICE " : "
#define UI_ORDER_REVIEW "You have selected \"%s\", \"%s\". This will cost you "PROTOTYPE_PRICE"."
#define UI_PAYMENT_START1 "Please hand over the money - type in the value of each note / coin in cents."
#define UI_PAYMENT_START2 "Press enter on a new and empty line to cancel this purchase:"
#define UI_PURCHASE_ENTER_ID "Please enter the ID of the item you with to purchase: "
#define UI_PURCHASE_NO_SUCH_ID "Item ID %s does not match any criteria!"
#define UI_PURCHASE_SUCCESS "Thank you! Here is your \"%s\", and your change of " PROTOTYPE_UNDERLINE_START "%s." PROTOTYPE_UNDERLINE_STOP NEWLINE "Please come back soon!"
#define UI_PURCHASE_NO_CHANGES "Thank you! Here is your \"%s\". Please come back soon!"
#define UI_FORCE_ABORT "Program was forced to be aborted! Unsaved data will be lost!"
#define UI_NEW_ITEM_ID_NOTIFY "This new meal item will have the Item ID %s."
#define UI_NEW_ITEM_NAME_ENTRY "Enter the item name: "
#define UI_NEW_ITEM_DESC_ENTRY "Enter the item description: "
#define UI_NEW_ITEM_PRICE_ENTRY "Enter the price for this item: "
#define UI_NEW_ITEM_ADD_SUCCESS "This item \"%s - %s\" has now been added to the menu."
#define UI_REMOVE_ITEM_ID "Enter the Item ID of the item to remove from the menu: "
#define UI_REMOVE_NO_ITEMS "Nothing to be deleted! There are no items in this vending machine."
#define UI_REMOVE_SUCCESS "\"%s - %s  %s\" has been removed from the system."
#define UI_REMOVE_NO_SUCH_ID "Item ID %s could not be found on this machine! Make sure you entered the ID correctly!"
#define UI_COIN_SUMMARY_HEADER1 "Coin Summary"
#define UI_COIN_SUMMARY_HEADER2 "---------------------"
#define UI_COIN_COL_1 "Denomination"
#define UI_COIN_COL_2 "Count"
#define UI_RESET_STOCK_CONFIRM "This option will reset all items on this vending machine to the default value, which is %d. Are you sure you have filled this machine? Select '1' if you are to ready to reset the stock. Other characters to cancel this operation. Choice: "
#define UI_RESET_STOCK_SUCCESS "All stock has been reset to the default level of %d"
#define UI_RESET_STOCK_CANCEL "Canceled reset stock operation."
#define UI_RESET_COIN_CONFIRM "This option will reset all coins into their default value, which is %d. Are you sure you have filled this machine? Select '1' if you are to ready to reset the stock. Other characters to cancel this operation. Choice: "
#define UI_RESET_COIN_SUCCESS "All coin count has been reset to the default level of %d"
#define UI_DISPLAY_ITEMS_ID "ID"
#define UI_DISPLAY_ITEMS_NAME "Name"
#define UI_DISPLAY_ITEMS_AVAILABLE "Available"
#define UI_DISPLAY_ITEMS_PRICE "Price"
#define UI_DISPLAY_ITEMS "Item Menu"
#define UI_RESET_COIN_CANCEL "Canceled reset stock operation."
#define UI_CLENING_UP "Cleaning up..."
#define UI_CLEAN_UP_FINISH "Clean up finished!"
#define UI_NO_CHANGES_AVAILABLE "Oops, sorry! The vending machine does not have the changes for you! It seems this transaction should be canceled. We apologise for the inconvenience."
#define MENU_DISPLAY "Display Items"
#define MENU_PURCHASE "Purchase Item"
#define MENU_SAVE "Save and Exit"
#define MENU_ADMIN_ADD_ITEM "Add an item"
#define MENU_ADMIN_REMOVE_ITEM "Remove an Item"
#define MENU_ADMIN_DISPLAY_COINS "Display Coins"
#define MENU_ADMIN_RESET_STOCKS "Reset Stocks"
#define MENU_ADMIN_RESET_COINS "Reset Coins"
#define MENU_EXIT "Abort Program"
#define MENU_UNKNOWN_MENU "Unknown menu selected..."

#define STARTING_ADMIN_MENU 3


/**
 * This is used to compensate for the extra character spaces taken up by
 * the '\n' and '\0' when input is read through fgets().
 **/
#define EXTRA_SPACES (NEW_LINE_SPACE + NULL_SPACE)
#define CHAR_NEWLINE '\n'
#define CHAR_NULL '\0'
#define NEWLINE "\n"
#define EMPTY_STRING ""
#define SPACE " "

/**
 * Call this function whenever you detect buffer overflow.
 **/
void readRestOfLine(void);

/**
 * Number Scanner
 * Declared by : Thomas Dwi Dinata
 * This function will return a number that is given by
 * the user from the Console. It also handles some
 * type errors and stream errors using given function
 * from the startup code.
 **/
int scanNumber(void);

/**
 * String Scanner
 * Declared by : Thomas Dwi Dinata
 * This function will receive string input from the User
 * and try to handle all overflow buffers and discard it.
 * It will return the clean strings with length of buffer
 * size.
 **/
char* scan(void);

/**
 * File Verification Function
 * Declared by : Thomas Dwi Dinata
 * This function will receive a FILE pointer, it will
 * check whether the file opened by the fopen() success
 * or not.
 **/
Boolean fileValid(FILE*);

/**
 * File to Console
 * Declared by : Thomas Dwi Dinata
 * This function is used to change the FILE pointer to
 * stdout, which is a console output for fallback if the file
 * is not available. It will return whether it is success
 * or not, but usually it must be success.
 * Important : DO NOT FORCE CLOSE stdout!!!
 **/
Boolean fileToConsole(FILE**);

/**
 * Close File
 * Declared by : Thomas Dwi Dinata
 * This function is used safely close the file pointer.
 * It will detect whether the received FILE* is a stdout or
 * just a file pointer. If stdout present, it will not
 * close it. It is better to use this prebuilt function
 * made by me to prevent closing stdout.
 **/
Boolean closeFile(FILE*);

/**
 * Broken / Corrupted File Detection
 * Declared by : Thomas Dwi Dinata
 * This function is used for detecting whether the file is
 * corrupted or not. An empty string on the CSV file like
 * are not acceptable by this program.
 **/
Boolean checkTokenised(char*);

/**
 * Universal Memory Free
 * Declared by : Thomas Dwi Dinata
 * This function is used to free the content of given pointer.
 * and it also NULLify the variable given in order to clear
 * the variable completely.
 **/
void freeThis(void**);



#endif
