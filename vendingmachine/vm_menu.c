/******************************************************************************
** Student name: 	Thomas Dwi Dinata
** Student number: 	s3702763
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "vm_menu.h"

/**
 * vm_menu.c handles the initialisation and management of the menu array.
 **/

/**
 * In this function you need to initialise the array of menu items
 * according to the text to be displayed for the menu. This array is
 * an array of MenuItem with text and a pointer to the function
 * that will be called.
 **/
void initMenu(MenuItem * menu)
{
    int i;
    MenuFunction originalMenu[MENU_DICTIONARY_SIZE] = {
        &displayItems,
        &purchaseItem,
        &saveAndExit,
        &addItem,
        &removeItem,
        &displayCoins,
        &resetStock,
        &resetCoins,
        &abortProgram
    };
    char* originalMenuText[MENU_DICTIONARY_SIZE] = {
        MENU_DISPLAY,
        MENU_PURCHASE,
        MENU_SAVE,
        MENU_ADMIN_ADD_ITEM,
        MENU_ADMIN_REMOVE_ITEM,
        MENU_ADMIN_DISPLAY_COINS,
        MENU_ADMIN_RESET_STOCKS,
        MENU_ADMIN_RESET_COINS,
        MENU_EXIT
    };
    i = 0;
    for(i = 0; i < MENU_DICTIONARY_SIZE ; ++i){
        menu->function = originalMenu[i];
        strcpy(menu->text, originalMenuText[i]);
        ++menu;
    }
    return;
}

/**
 * Gets input from the user and returns a pointer to the MenuFunction
 * that defines how to perform the user's selection. NULL is returned
 * if an invalid option is entered.
 **/
MenuFunction getMenuChoice(MenuItem * menu)
{
    MenuFunction* extract = NULL;
    if(menu->function != NULL)
        extract = &menu->function;
    return *extract;
}

Boolean isBack(char* check){
    if( strcmp(check, "") == 0 )
        return TRUE;
    else
        return FALSE;
}
