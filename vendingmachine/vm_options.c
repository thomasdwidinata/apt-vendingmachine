/******************************************************************************
** Student name: 	Thomas Dwi Dinata
** Student number: 	s3702763
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "vm_options.h"

/**
 * vm_options.c this is where you need to implement the system handling
 * functions (e.g., init, free, load, save) and the main options for
 * your program. You may however have some of the actual work done
 * in functions defined elsewhere.
 **/

/**
 * Initialise the system to a known safe state. Look at the structure
 * defined in vm_system.h.
 **/
Boolean systemInit(VmSystem * system)
{
    Boolean success = FALSE;

    if(system != NULL)
        success = TRUE;
	system->itemList = NULL;
    system->itemList = malloc(sizeof(List));
    if(system->itemList == NULL)
        success = FALSE;
    system->itemList->size = 0;
    system->itemList->head = NULL;
    system->coinFileName = NULL;
    system->stockFileName = NULL;
    return success;
}

/**
 * Free all memory that has been allocated. If you are struggling to
 * find all your memory leaks, compile your program with the -g flag
 * and run it through valgrind.
 **/
void systemFree(VmSystem * system)
{
    Node* previous = NULL;
    Node* current = system->itemList->head;
    while(current != NULL)
    {
        previous = current;
	current = current->next;
        free(previous->data);
        free(previous);
    }
    free(system->itemList);
    system->itemList = NULL;
    current = NULL;
    free(system);
    system = NULL;
    return;
}

/**
 * Loads the stock and coin data into the system. You will also need to assign
 * the char pointers to the stock and coin file names in the system here so
 * that the same files will be used for saving. A key part of this function is
 * validation. A substantial number of marks are allocated to this function.
 **/
Boolean loadData(
    VmSystem * system, const char * stockFileName, const char * coinsFileName)
{
    if(loadStock(system, stockFileName) && loadCoins(system, coinsFileName))
        return TRUE;
    else
        return FALSE;
}

/**
 * Loads the stock file data into the system.
 **/
Boolean loadStock(VmSystem * system, const char * fileName)
{
    /* Caches Declaration */
    char* cacheString = NULL;
    char* buffer = malloc(DESC_LEN + NULL_SPACE);
    char* priceCache = malloc(DESC_LEN + NULL_SPACE);

    /* File variables */
    FILE* stockFile = NULL;
    Boolean fileValid = FALSE;

    /* Tokenising stuffs */
    char* token = NULL;

    /* Objects */
    Stock* currentStock = NULL;
    Price price = {0,0};

    stockFile = fopen(fileName, READ);
    if(stockFile == NULL){
        printf(SYSTEM_FILE_ERROR NEWLINE, system->stockFileName);
        fileValid = FALSE;
    }
    else{
        /* Read the file line by line, and try to tokenise it! */
        /*while((readStatus = getline(&buffer, &bufferSize, stockFile)) != -1) {*/
        while(fgets(buffer, DESC_LEN, stockFile) != NULL){
            currentStock = malloc(sizeof(Stock));

            /* ID */
            token = strtok(buffer, STOCK_DELIM);
            if(!checkTokenised(token)){
                printf(SYSTEM_FILE_LINE_CORRUPT NEWLINE, buffer);
                continue;
            } else if( strlen(token) > ID_LEN+NULL_SPACE){
                printf(SYSTEM_FILE_LINE_CORRUPT NEWLINE, buffer);
                continue;
            }
                
            strcpy(currentStock->id, token);

            /* Name */
            token = strtok(NULL, STOCK_DELIM);
            if(!checkTokenised(token)){
                printf(SYSTEM_FILE_LINE_CORRUPT NEWLINE, buffer);
                continue;
            } else if( strlen(token) > NAME_LEN+NULL_SPACE){
                printf(SYSTEM_FILE_LINE_CORRUPT NEWLINE, buffer);
                continue;
            }
            strcpy(currentStock->name, token);

            /* Description */
            token = strtok(NULL, STOCK_DELIM);
            if(!checkTokenised(token)){
                printf(SYSTEM_FILE_LINE_CORRUPT NEWLINE, buffer);
                continue;
            }else if( strlen(token) > DESC_LEN+NULL_SPACE){
                printf(SYSTEM_FILE_LINE_CORRUPT NEWLINE, buffer);
                continue;
            }
            strcpy(currentStock->desc, token);

            /* Price (Add to temporary storage first */
            token = strtok(NULL, STOCK_DELIM);
            if(!checkTokenised(token)){
                printf(SYSTEM_FILE_LINE_CORRUPT NEWLINE, buffer);
                continue;
            }else if( strlen(token) > PRICE_LEN+NULL_SPACE){
                printf(SYSTEM_FILE_LINE_CORRUPT NEWLINE, buffer);
                continue;
            }
            strcpy(priceCache, token);

            /* onHand */
            token = strtok(NULL, STOCK_DELIM);
            if(!checkTokenised(token)){
                printf(SYSTEM_FILE_LINE_CORRUPT NEWLINE, buffer);
                continue;
            }
            currentStock->onHand = (int) strtol(token, &cacheString, NUMBER_BASE);

            /* Price Object Modification Begins */
            token = strtok(priceCache, COIN_DELIM_REAL_LIFE);
            if(!checkTokenised(token)){
                printf(SYSTEM_FILE_LINE_CORRUPT NEWLINE, buffer);
                continue;
            }
            price.dollars = (unsigned int) strtol(token, &cacheString, NUMBER_BASE);
            token = strtok(NULL, COIN_DELIM_REAL_LIFE);
            if(token != NULL)
                price.cents = (unsigned int) strtol(token, &cacheString, NUMBER_BASE);
            else
                price.cents = 0;

            currentStock->price = price;

            /* Insert into Linked List */
            addStock(system, currentStock);
        }
        fileValid = TRUE;
    }
    system->stockFileName = fileName;
    free(buffer);
    free(priceCache);
    fclose(stockFile);
    return fileValid;
}

/**
 * Loads the coin file data into the system.
 **/
Boolean loadCoins(VmSystem * system, const char * fileName)
{
    int denom = 0;
    int count = 0;
    char buffer[DESC_LEN+NULL_SPACE];
    char* cache = NULL;
    char* tokenised = NULL;
    FILE* fptr = NULL;
    Boolean success = FALSE;
    initCoin(system);
    fptr = fopen(fileName, READ);
    if(fileValid(fptr)){
        while(fgets(buffer,  DESC_LEN+NULL_SPACE, fptr) != NULL){
            tokenised = strtok(buffer, COIN_DELIM);
            if(tokenised == NULL){
                printf(SYSTEM_FILE_LINE_CORRUPT NEWLINE, buffer);
                continue;
            }
            denom = (int) strtol(tokenised, &cache, NUMBER_BASE);
            tokenised = strtok(NULL, COIN_DELIM);
            if(tokenised == NULL){
                printf(SYSTEM_FILE_LINE_CORRUPT NEWLINE, buffer);
                continue;
            }
            count = (int) strtol(tokenised, &cache, NUMBER_BASE);

            addCoin(system, toDenom(denom), count);
        }
        system->coinFileName = fileName;
        success = TRUE;
    }
    else
        printf(SYSTEM_FILE_ERROR, system->coinFileName);
    fclose(fptr);    
    return success;
}

/**
 * Saves all the stock back to the stock file.
 **/
Boolean saveStock(VmSystem * system)
{
    Boolean success = FALSE;

    Node* current = NULL;
    FILE* fptr = fopen(system->stockFileName, WRITE);
    /* If the program does not able to open the file, go into fallback mode, which prints to the console */
    if(!fileValid(fptr)){
        printf(SYSTEM_FILE_ERROR, system->stockFileName);
        fileToConsole(&fptr); /* Switch mode to Console Output, refer to utility.h */
    }
    if(system->itemList->size == 0){
        printf(UI_NO_ITEM_WARNING NEWLINE);
        success = TRUE;
    } else {
        current = system->itemList->head;

        /* Print the linked list into the program */
        while(current != NULL)
        {
            fprintf(fptr, PROTOTYPE_STOCK_FILE NEWLINE,current->data->id, current->data->name, current->data->desc, current->data->price.dollars, current->data->price.cents, current->data->onHand);
            current = current->next;
        }
        success = TRUE;
    }
    fclose(fptr);
    fptr = NULL;

    return success;
}

/**
 * Saves all the coins back to the coins file.
 **/
Boolean saveCoins(VmSystem * system)
{
    Boolean success = TRUE;

    int iterator = 0;

    FILE* fptr = fopen(system->coinFileName, WRITE);
    if(!fileValid(fptr)){
        printf(SYSTEM_FILE_ERROR, system->coinFileName);
        fileToConsole(&fptr);
    }
    for(iterator = 0; iterator < NUM_DENOMS; ++iterator){
        fprintf(fptr, PROTOTYPE_COIN_FILE NEWLINE, coinParse(system->cashRegister[iterator].denom), system->cashRegister[iterator].count);
    }

    closeFile(fptr);

    return success;
}

/**
 * This option allows the user to display the items in the system.
 * This is the data loaded into the linked list in the requirement 2.
 **/
void displayItems(VmSystem * system)
{
    Node* current = NULL;
    int iteration = 0;

    if(system->itemList->size < 1){
        printf(UI_NO_ITEM NEWLINE);
        return;
    }

    current = system->itemList->head;

    /* Print the Header */
    printf(UI_DISPLAY_ITEMS NEWLINE);
    printf(PROTOTYPE_STOCK_PRINT_HEADER1 NEWLINE, UI_DISPLAY_ITEMS_ID, UI_DISPLAY_ITEMS_NAME, UI_DISPLAY_ITEMS_AVAILABLE, UI_DISPLAY_ITEMS_PRICE);
    for(iteration = 0; iteration < 46; ++iteration)
        printf(UI_TABLE_HEADER);
    printf(NEWLINE);
    while(current != NULL)
    {
        printf(PROTOTYPE_STOCK_PRINT_ITEMS NEWLINE,
               current->data->id,
               current->data->name,
               current->data->onHand,
               current->data->price.dollars,
               current->data->price.cents);
        current = current->next;
    }
    printf(NEWLINE);
}

/**
 * This option allows the user to purchase an item.
 * This function implements requirement 5 of the assignment specification.
 **/
void purchaseItem(VmSystem * system)
{
    char* rawInput = malloc(BUFFER_SIZE);
    char* choice = NULL;
    char* tokenCache = NULL;
    int price = 0;
    int givenAmount = 0;
    char* change;
    Stock* selected = NULL;

    if(system->itemList->size < 1){
        printf(UI_NO_ITEM NEWLINE);
        return;
    }

    printf(UI_PURCHASE_ITEM_HEADER NEWLINE UI_PURCHASE_ENTER_ID);
    readRestOfLine();
    choice = scan();

    if(isBack(choice)){
        printf(UI_CANCEL_OPERATION NEWLINE);
        free(rawInput);
        free(choice);
        return;
    }

    selected = getStock(system, choice);
    if(selected == NULL)
    {
        printf(UI_PURCHASE_NO_SUCH_ID NEWLINE, choice);
        free(rawInput);
        free(choice);
        return;
    }
    price = (selected->price.dollars * DOLLAR_CONVERT_TO_DECIMAL) + (selected->price.cents);

    printf(UI_ORDER_REVIEW NEWLINE UI_PAYMENT_START1 NEWLINE UI_PAYMENT_START2 NEWLINE,
           selected->name,selected->desc,selected->price.dollars,selected->price.cents);
    while(price > 0)
    {
	free(choice);
	choice = NULL;
        printf(UI_PAYMENT_INCOMPLETE, (int) (price / DOLLAR_CONVERT_TO_DECIMAL),
               (int) (price % DOLLAR_CONVERT_TO_DECIMAL));
        choice = scan();
        if(isBack(choice)){
            printf(UI_CANCEL_OPERATION NEWLINE);
            free(rawInput);
            free(choice);
            return;
        }
        strcpy(rawInput, choice);
        tokenCache = strtok(choice, COIN_DELIM_REAL_LIFE);
        givenAmount = parseGivenAmount(tokenCache, choice);

        switch(givenAmount){
            default:
                printf(UI_DENOM_ERROR NEWLINE, rawInput);
                break;
            case DECIMAL_DENOM_5CENT:
            case DECIMAL_DENOM_10CENT:
            case DECIMAL_DENOM_20CENT:
            case DECIMAL_DENOM_50CENT:
            case DECIMAL_DENOM_1DOLLAR:
            case DECIMAL_DENOM_2DOLLAR:
            case DECIMAL_DENOM_5DOLLAR:
            case DECIMAL_DENOM_10DOLLAR:
                price -= givenAmount;
                incrementCoin(system , toDenom(givenAmount));
                break;
        }
    }
    
    --selected->onHand;
    price *= (NEGATIVE_NEGATION);
    change = calculateChange(system, price);
    if(strcmp(change, SYSTEM_COIN_DISPENSE_ERROR) == 0)
        printf(UI_NO_CHANGES_AVAILABLE NEWLINE);
    else if(strcmp(change, SYSTEM_COIN_DISPENSE_NO_CHANGES) == 0)
        printf(UI_PURCHASE_NO_CHANGES NEWLINE, selected->name);
    else
        printf(UI_PURCHASE_SUCCESS NEWLINE, selected->name, UNDERLINE_PARAMETER, change, UNDERLINE_PARAMETER);
    free(rawInput);
    free(choice);
    free(change);
    return;
}

/**
 * You must save all data to the data files that were provided on the command
 * line when the program loaded up, display goodbye and free the system.
 * This function implements requirement 6 of the assignment specification.
 **/
void saveAndExit(VmSystem * system)
{
    printf(SYSTEM_SAVING NEWLINE);
    if(saveStock(system) && saveCoins(system)){
        printf(SYSTEM_SAVE_SUCCESS NEWLINE);
    } else
        printf(SYSTEM_SAVE_FAILURE NEWLINE);
    systemFree(system);
}

/**
 * This option adds an item to the system. This function implements
 * requirement 7 of of assignment specification.
 **/
void addItem(VmSystem * system)
{
    char* itemName = NULL;
    char* itemDesc = NULL;
    char* itemPriceCache = NULL;
    int itemPrice = 0;
    int itemPriceDollar = 0;
    int itemPriceCent = 0;
    char* tokenCache = NULL;
    Stock* newStock = malloc(sizeof(Stock));

    char id[ID_LEN+NULL_SPACE]; /* Used to store temporary / rubbish value of character I on the Item ID */
    char* cache;
    cache = getNextAvailableId(system);
    strcpy(id, cache);
    free(cache);
    
    printf(UI_NEW_ITEM_ID_NOTIFY NEWLINE ,id);
    printf(UI_NEW_ITEM_NAME_ENTRY);
    readRestOfLine();
    itemName = scan();
    if(isBack(itemName)){
        printf((UI_CANCEL_OPERATION NEWLINE));
        return;
    }
    printf(UI_NEW_ITEM_DESC_ENTRY);
    itemDesc = scan();
    if(isBack(itemDesc)){
        printf((UI_CANCEL_OPERATION NEWLINE));
        return;
    }
    printf(UI_NEW_ITEM_PRICE_ENTRY);
    itemPriceCache = scan();
    if(isBack(itemPriceCache)){
        printf((UI_CANCEL_OPERATION NEWLINE));
        return;
    }
    /* Parse the price, either input 2.00 or 200 entered, no need to check coin denoms */
    tokenCache = strtok(itemPriceCache, COIN_DELIM_REAL_LIFE);
    if(tokenCache == NULL) /* Invalid number input */
    {
        printf(UI_DENOM_ERROR NEWLINE, tokenCache);
        return;
    }
    itemPrice = parseGivenAmount(tokenCache, itemPriceCache);
    itemPriceDollar = (int) (itemPrice / DOLLAR_CONVERT_TO_DECIMAL);
    itemPriceCent = (int) (itemPrice % DOLLAR_CONVERT_TO_DECIMAL);

    /* Begin inserting to the linked list. With the default stock defined on the header file */
    strcpy(newStock->id, id);
    strcpy(newStock->name, itemName);
    strcpy(newStock->desc, itemDesc);
    newStock->onHand = DEFAULT_STOCK_LEVEL;
    newStock->price.dollars = itemPriceDollar;
    newStock->price.cents = itemPriceCent;

    /* Insert into the linked list */
    addStock(system, newStock);
    printf(UI_NEW_ITEM_ADD_SUCCESS NEWLINE, itemName, itemDesc);
    free(itemName);
    itemName = NULL;
    free(itemDesc);
    itemDesc = NULL;
    free(itemPriceCache);
    itemPriceCache = NULL;
}

/**
 * Remove an item from the system, including free'ing its memory.
 * This function implements requirement 8 of the assignment specification.
 **/
void removeItem(VmSystem * system)
{
    char* id = NULL;
    Node* current = NULL;
    Stock* stock = NULL;
    Boolean success = FALSE;
    printf(UI_REMOVE_ITEM_ID);
    readRestOfLine();
    id = scan();
    if(isBack(id))
    {
        printf(UI_CANCEL_OPERATION NEWLINE);
        return;
    }
    current = system->itemList->head;
    if(system->itemList->size < 1){
        printf(UI_REMOVE_NO_ITEMS NEWLINE);
        success = FALSE;
    } else {
        while(current != NULL){
            if(strcmp(current->data->id, id) == 0){
                stock = current->data;
                success = TRUE;
                break;
            }
            current = current->next;
        }if(success){
            printf(UI_REMOVE_SUCCESS NEWLINE, current->data->id, current->data->name, current->data->desc);
            deleteStock(system, stock);
        }
        else
            printf(UI_REMOVE_NO_SUCH_ID NEWLINE, id);
    }
    free(id);
    id = NULL;
}

/**
 * This option will require you to display the coins from lowest to highest
 * value and the counts of coins should be correctly aligned.
 * This function implements part 4 of requirement 18 in the assignment
 * specifications.
 **/
void displayCoins(VmSystem * system)
{
    int iteration = 0;
    char* cache = NULL;
    printf(UI_COIN_SUMMARY_HEADER1 NEWLINE);
    printf(UI_COIN_SUMMARY_HEADER2 NEWLINE);
    printf(PROTOTYPE_COIN_SUMMARY_TABLE_HEADER NEWLINE, UI_COIN_COL_1, UI_COIN_COL_2);
    for(iteration = 0; iteration < NUM_DENOMS; ++iteration){
	cache = coinParseDecimal(system->cashRegister[iteration].denom);
        printf(PROTOTYPE_COIN_SUMMARY_TABLE_CONTENT NEWLINE,cache, system->cashRegister[iteration].count);
	free(cache);
	cache = NULL;    
    }

}

/**
 * This option will require you to iterate over every stock in the
 * list and set its onHand count to the default value specified in
 * the startup code.
 * This function implements requirement 9 of the assignment specification.
 **/
void resetStock(VmSystem * system)
{
    int choice = 0;
    if(system->itemList->size < 1){
        printf(UI_NO_ITEM NEWLINE);
        return;
    }
    printf(UI_RESET_STOCK_CONFIRM, DEFAULT_STOCK_LEVEL);
    choice = scanNumber();
    if(choice == 1)
    {
        resetAllStock(system);
        printf(UI_RESET_STOCK_SUCCESS NEWLINE, DEFAULT_STOCK_LEVEL);
    } else
        printf(UI_RESET_STOCK_CANCEL NEWLINE);
}

/**
 * This option will require you to iterate over every coin in the coin
 * list and set its 'count' to the default value specified in the
 * startup code.
 * This requirement implements part 3 of requirement 18 in the
 * assignment specifications.
 **/
void resetCoins(VmSystem * system)
{
    int choice = 0;
    printf(UI_RESET_COIN_CONFIRM, DEFAULT_COIN_COUNT);
    choice = scanNumber();
    if(choice == 1)
    {
        resetAllCoins(system);
        printf(UI_RESET_COIN_SUCCESS NEWLINE, DEFAULT_COIN_COUNT);
    } else
        printf(UI_RESET_COIN_CANCEL NEWLINE);
}

/**
 * This option will require you to display goodbye and free the system.
 * This function implements requirement 10 of the assignment specification.
 **/
void abortProgram(VmSystem * system)
{
    printf(UI_FORCE_ABORT NEWLINE);
    printf(UI_CLENING_UP NEWLINE);
    systemFree(system);
    printf(UI_CLEAN_UP_FINISH NEWLINE);
    printf(UI_EXIT NEWLINE);
}
