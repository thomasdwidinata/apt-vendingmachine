/******************************************************************************
** Student name: 	Thomas Dwi Dinata
** Student number: 	s3702763
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#ifndef VM_H
#define VM_H

#include "vm_menu.h"
#include "vm_stock.h"
#include "utility.h"

Boolean checkArgument(int, char**);
void printInstruction(void);

#endif
