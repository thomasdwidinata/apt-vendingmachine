/******************************************************************************
** Student name: 	Thomas Dwi Dinata
** Student number: 	s3702763
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#ifndef VM_COIN_H
#define VM_COIN_H

#include "vm_system.h"

/**
 * The default coin level used when resetting values.
 **/
#define DEFAULT_COIN_COUNT 20
#define DOLLAR_CONVERT_TO_DECIMAL 100
#define COIN_DELIM ","
#define COIN_DELIM_REAL_LIFE "."
#define DECIMAL_DENOM_50CENT 50
#define DECIMAL_DENOM_20CENT 20
#define DECIMAL_DENOM_10CENT 10
#define DECIMAL_DENOM_5CENT 5
#define DECIMAL_DENOM_1DOLLAR 100
#define DECIMAL_DENOM_2DOLLAR 200
#define DECIMAL_DENOM_5DOLLAR 500
#define DECIMAL_DENOM_10DOLLAR 1000
#define ADD_ONE_COIN 1

/**
 * Coin Parser (Non-Decimal)
 * Declared by : Thomas Dwi Dinata
 * This function will parse from the Coin Enum to the real coins
 * and translate it into decimal. (e.g. 550)
 **/
int coinParse(Denomination);

/**
 * Coin Parser (Decimal)
 * Declared by : Thomas Dwi Dinata
 * This function will parse from the Coin Enum to the real coins
 * and translate it into decimal. (e.g. 5.50)
 **/
char* coinParseDecimal(Denomination);

/**
 * Coin Parser from String
 * Declared by : Thomas Dwi Dinata
 * This function will parse from any string into integer
 * number of coin.
 **/
int parseGivenAmount(char*, char*);

/**
 * Coin Reset
 * Declared by : Thomas Dwi Dinata
 * This function will reset the coin counts in the System.
 **/
void resetAllCoins(VmSystem*);

/**
 * Calculate Changes
 * Declared by : Thomas Dwi Dinata
 * This function will determine which coins should be used
 * for giving changes to the customer. It will return the dollar
 * string.
 **/
char* calculateChange(VmSystem*, int);

/**
 * Add Coin Count
 * Declared by : Thomas Dwi Dinata
 * Adds coin count to the system. Used only on first
 * initialisation of the program.
 **/
void addCoin(VmSystem*, Denomination, int);

/**
 * Initialise VmSystem Coin Array
 * Declared by : Thomas Dwi Dinata
 * Initialise the coin system on the VmSystem. Initialised
 * with default denomintaion.
 **/
void initCoin(VmSystem*);

/**
 * Add Coin Count
 * Declared by : Thomas Dwi Dinata
 * Deducts a coin denomintaion. Because it is going to be
 * dispensed to the customer. Returns false if no more
 * coins to be dispensed
 **/
Boolean dispenseCoin(VmSystem*, Denomination, int);

/**
 * Determine Coin
 * Declared by : Thomas Dwi Dinata
 * This function is to determine and parse from the coin decimal
 * to Denomination Enumeration.
 **/
Denomination toDenom(int);

/**
 * Increment Coin by One
 * Declared by : Thomas Dwi Dinata
 * This function is for incrementing a given coin by one.
 **/
void incrementCoin(VmSystem*, Denomination);

#endif
