/******************************************************************************
** Student name: 	Thomas Dwi Dinata
** Student number: 	s3702763
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "vm.h"

int main(int argc, char ** argv)
{
    /* Main Code */
    Boolean play = TRUE;
    int i = 0;
    unsigned int choice = 0;
    VmSystem* vm;
    MenuItem menu[MENU_DICTIONARY_SIZE];
    play = checkArgument(argc, argv);
    
    vm = malloc(sizeof(VmSystem));
    if((!systemInit(vm)) || vm == NULL){
        printf(SYSTEM_MALLOC_FAIL NEWLINE);
        return EXIT_FAILURE;
    }
    if(!loadData(vm, argv[1], argv[2])){
        printf(SYSTEM_INIT_FAILED NEWLINE);
        return EXIT_FAILURE;
    }
    initMenu(menu);
    while(play){
        printf(UI_WELCOME);
        for(i = 0; i < MENU_DICTIONARY_SIZE; ++i){
            if(i == STARTING_ADMIN_MENU)
                printf(UI_ADMIN_ONLY NEWLINE);
            printf(PROTOTYPE_MAINMENU NEWLINE, i+1, menu[i].text);
        }
        while(choice < 1 || choice > 9)
        {
            printf(UI_SELECT_MENU);
            choice = scanNumber();
            if(choice < 1 || choice > 9)
                printf(UI_INVALID_CHOICE NEWLINE);
        }
        
        if(choice == MENU_SAVE_EXIT || choice == MENU_FORCE_EXIT)
            play = FALSE;
        else if( choice < MENU_START_INDEX || choice >= MENU_DICTIONARY_SIZE)
        {
            printf(MENU_UNKNOWN_MENU NEWLINE);
            continue;
        }
        getMenuChoice(&menu[choice-INDEX_OFFSET_DECREMENT_ONE])(vm);
        choice = 0;
    }
    
    return EXIT_SUCCESS;
}

Boolean checkArgument(int argc, char** argv)
{
    Boolean validity = TRUE;
    switch(argc)
    {
        default:
            validity = FALSE;
            printInstruction();
            break;
        case 3: /* First arg. is the pwd, Second arg. is stock file, Third arg. is coin file */
            break;
    }
    return validity;
}

void printInstruction(void)
{
    printf(UI_USAGE);
}
