/******************************************************************************
** Student name: 	Thomas Dwi Dinata
** Student number: 	s3702763
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "vm_stock.h"

/**
 * vm_stock.c this is the file where you will implement the
 * interface functions for managing the stock list.
 **/

/**
 * Some example functions:
 * create list, free list, create node, free node, insert node, etc...
 */

Boolean addStock(VmSystem* vm, Stock* newStock){
    Boolean success = FALSE;
    Node* current = NULL;
    Node* before = NULL;
    Node* new = NULL;
    
    if(vm->itemList->head == NULL){
        /* Initialise Node if empty */
        vm->itemList->head = malloc(sizeof(Node));
        if(vm->itemList->head != NULL)
            success = TRUE;
        vm->itemList->size = 0;
        
        /* After initialising Node, insert the new stock */
        vm->itemList->head->data = newStock;
        vm->itemList->head->next = NULL;
        vm->itemList->size++;
    } else { /* If the head exists, just continue adding */
        current = vm->itemList->head;
        before = NULL;
        while(current != NULL) {
            if(strcmp(current->data->name, newStock->name) > 0){
                if(before == NULL)
                {
                    new = malloc(sizeof(Node));
                    if(new != NULL)
                        success = TRUE;
                    new->next = vm->itemList->head;
                    new->data = newStock;
                    vm->itemList->head = new;
                    break;
                }else{
                    new = malloc(sizeof(Node));
                    if(new != NULL)
                        success = TRUE;
                    new->data = newStock;
                    new->next = before->next;
                    before->next = new;
                    vm->itemList->size++;
                    break;
                }
                break;
            } else if(current->next == NULL){
                current->next = malloc(sizeof(Node));
                if(current->next != NULL)
                    success = TRUE;
                current->next->data = newStock;
                current->next->next = NULL;
                vm->itemList->size++;
                break;
            } else {
                before = current;
                current = current->next;
            }
        }
        
    }
    return success;
}

void deleteStock(VmSystem* vm, Stock* parameter){
    Node* current = NULL;
    Node* delete = NULL;
    
    if(parameter == NULL)
        return;
    
    current = vm->itemList->head;
    while(current != NULL){
        if(current->next != NULL){
            if(strcmp(current->next->data->id, parameter->id) == 0){
                delete = current->next;
                current->next = current->next->next;
                break;
            } else if( (strcmp(current->data->id, parameter->id)) == 0){
                /* If the head was the culprit, it has special instruction */
                delete = current;
                vm->itemList->head = current->next;
                break;
            }
        }
        current = current->next;
    }
    if(delete != NULL){
        free(delete->data);
        free(delete);
    }
    --vm->itemList->size;
}

Stock* getStock(VmSystem* vm, char* Id){
    Node* current = NULL;
    Stock* search = NULL;
    
    current = vm->itemList->head;
    while(current != NULL){
        if(strcmp(current->data->id, Id) == 0){
            search = current->data;
            break;
        }
        current = current->next;
    }
    return search;
}

void resetAllStock(VmSystem* vm){
    Node* current = vm->itemList->head;
    while(current != NULL){
        current->data->onHand = DEFAULT_STOCK_LEVEL;
        current = current->next;
    }
}

char* getNextAvailableId(VmSystem* vm){
    Node* current = vm->itemList->head;
    char currentIdPrefix[ID_LEN + NULL_SPACE];
    int* IDs = malloc(BUFFER_SIZE * sizeof(int));
    int iterator[2] = {0, 0};
    int cache = 0;
    int currentIndex = 0;
    char* toBeReturned = malloc(ID_LEN + NULL_SPACE);
    if(vm->itemList->size < 1)
        return PROTOTYPE_FIRST_ID;
    memset(IDs, 0, BUFFER_SIZE);
    while(current != NULL){
        if( (sizeof(IDs) / sizeof(int)) == BUFFER_SIZE )
            IDs = realloc(IDs, LARGE_BUFFER * sizeof(int));
        sscanf(current->data->id, PROTOTYPE_ID_GET, currentIdPrefix, &IDs[currentIndex++]);
        current = current->next;
    }
    
    for(iterator[0] = 0; iterator[0] < currentIndex+1; ++iterator[0]){
        for(iterator[1] = 0; iterator[1] < (currentIndex-iterator[0]-1); ++iterator[1]){
            if(IDs[iterator[1]] > IDs[iterator[1]+1]){
                cache = IDs[iterator[1]];
                IDs[iterator[1]] = IDs[iterator[1]+1];
                IDs[iterator[1]+1] = cache;
            }
        }
    }
    
    if(IDs[0] != 1)
        sprintf(currentIdPrefix, PROTOTYPE_ID_NEW, 1 );
    else
        for(iterator[0] = 0; iterator[0] < currentIndex+1; ++iterator[0]){
            if( (IDs[iterator[0]] - IDs[iterator[0]+1]) < -1 || (IDs[iterator[0]] - IDs[iterator[0]+1]) > 0){
                sprintf(currentIdPrefix, PROTOTYPE_ID_NEW, (IDs[iterator[0]+1] + (IDs[iterator[0]] - IDs[iterator[0]+1]) + 1) );
                break;
            }
        }
    free(IDs);
    strcpy(toBeReturned, currentIdPrefix);
    return toBeReturned;
}
