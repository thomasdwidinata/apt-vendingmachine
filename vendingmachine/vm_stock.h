/******************************************************************************
** Student name: 	Thomas Dwi Dinata
** Student number: 	s3702763
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#ifndef VM_STOCK_H
#define VM_STOCK_H

#include "vm_coin.h"
#include "vm_options.h"

/**
 * The default stock level that all new stock should start with and the value
 * to use when restting stock.
 **/
#define DEFAULT_STOCK_LEVEL 20

#define STOCK_DELIM "|"

/*
 * Stock Linked List Implementation
 * Declared By : Thomas Dwi Dinata
 * All linked list operations are declared here such as Add, Delete,
 * and Getting. It also has reseting stock to its default level.
 * Also it has get next available ID for inserting purpose.
 */
Boolean addStock(VmSystem*,Stock*);
void deleteStock(VmSystem*,Stock*);
Stock* getStock(VmSystem*,char*);
void resetAllStock(VmSystem*);
char* getNextAvailableId(VmSystem*);

#endif
