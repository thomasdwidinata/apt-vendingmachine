/******************************************************************************
** Student name: 	Thomas Dwi Dinata
** Student number: 	s3702763
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "utility.h"

/**
 * Function required to be used when clearing the buffer. It simply reads
 * each char from the buffer until the buffer is empty again. Please refer
 * to the materials on string and buffer handling in the course for more
 * information.
 **/
void readRestOfLine()
{
    int ch;
    while(ch = getc(stdin), ch != EOF && ch != '\n')
    { } /* Gobble each character. */

    /* Reset the error status of the stream. */
    clearerr(stdin);
}

int scanNumber(void)
{
    int num = 0;
    int test = EOF;
    while(test == EOF)
    {
        /* scanf used because it can easily scan numbers only and check
         * any wrong input detected on buffer
         */
        test = scanf("%d", &num);
        /* On buffer error, we need to flush it! readRestOfLine() function
         * saves the day! */
        if(test == EOF)
            readRestOfLine();
        if(test == 0)
            readRestOfLine();
        else /* If nothing is wrong, return the number */
            return num;
    }
    return num;
}

char* scan(void){
    char choice[BUFFER_SIZE+NULL_SPACE];
    char* toBeReturned = malloc(BUFFER_SIZE+NULL_SPACE);
    char* cache = NULL;
    int iterator = 0;
    if(fgets(choice, BUFFER_SIZE, stdin))
    {
        /* If the input was bigger than the buffer, cut it out! */
        if((cache = strchr(choice, CHAR_NEWLINE))){
            *cache = 0;
        } else {
            scanf(PROTOTYPE_SCANF_BUFFER_FIX_1);
            scanf(PROTOTYPE_SCANF_BUFFER_FIX_2);
        }
    }
    /* Delete the new line (\n) character */
    for(iterator = 0; choice[iterator]; iterator++){
        if(choice[iterator] == CHAR_NEWLINE)
            choice[iterator] = CHAR_NULL;
    }
    strcpy(toBeReturned, choice);
    return toBeReturned;
}

Boolean fileValid(FILE* fp){
    if(fp == NULL) /* If the program does not able to open the file, go into fallback mode, which prints to the console */
        return FALSE;
    else
        return TRUE;
}

Boolean fileToConsole(FILE** fp){
    if(fp != NULL)
        fclose(*fp);
    *fp = stdout;
    return TRUE;
}

Boolean closeFile(FILE* fp){
    if(fp != stdout || fp != NULL)
        fclose(fp);
    if(fp == NULL)
        return TRUE;
    else
        return FALSE;
}

Boolean checkTokenised(char* check){
    Boolean success = FALSE;
    if(check == NULL)
        success = FALSE;
    if(strcmp(check, "") == 0)
        success = FALSE;
    else
        success = TRUE;
    return success;
}

void freeThis(void** ptr){
    if(*ptr != NULL)
        free(*ptr);
    *ptr = NULL;
}
