/******************************************************************************
** Student name: 	Thomas Dwi Dinata
** Student number: 	s3702763
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "vm_coin.h"

/**
 * Implement functions here for managing coins and the
 * "cash register" contained in the VmSystem struct.
 **/

/**
 * Some example functions:
 * init coins array, insert coin, change coin count for Denomination,
 * convert Denomination to coin value, deducting coins from register, etc...
 */

int coinParse(Denomination denom){
    switch(denom){
        case FIVE_CENTS:
            return DECIMAL_DENOM_5CENT;
        case TEN_CENTS:
            return DECIMAL_DENOM_10CENT;
        case TWENTY_CENTS:
            return DECIMAL_DENOM_20CENT;
        case FIFTY_CENTS:
            return DECIMAL_DENOM_50CENT;
        case ONE_DOLLAR:
            return DECIMAL_DENOM_1DOLLAR;
        case TWO_DOLLARS:
            return DECIMAL_DENOM_2DOLLAR;
        case FIVE_DOLLARS:
            return DECIMAL_DENOM_5DOLLAR;
        case TEN_DOLLARS:
            return DECIMAL_DENOM_10DOLLAR;
        default:
            return 0;
    }
}

char* coinParseDecimal(Denomination denom){
    int coin = coinParse(denom);
    char* coinDecimal = malloc(BUFFER_SIZE);
    sprintf(coinDecimal, PROTOTYPE_PRICE, (int) (coin / DOLLAR_CONVERT_TO_DECIMAL), (int) (coin % DOLLAR_CONVERT_TO_DECIMAL) );
    return coinDecimal;
}

int parseGivenAmount(char* tokenCache, char* rawData){
    char* endptr = NULL;
    int given = 0;
    if(tokenCache == NULL)
        given = 0;
    else {
        given = ((int) strtol(tokenCache,&endptr,NUMBER_BASE)) * DOLLAR_CONVERT_TO_DECIMAL;
        tokenCache = strtok(NULL, COIN_DELIM_REAL_LIFE);
        if(tokenCache == NULL) /* If only decimal given */
            given = (int) (strtol(rawData, &endptr, NUMBER_BASE));
        else /* Not decimal given (e.g. 5.00) */
            given += (int) strtol(tokenCache, &endptr, NUMBER_BASE);
    }
    return given;
}

void resetAllCoins(VmSystem* vm){
    int i = 0;
    for(i = 0; i < NUM_DENOMS; ++i)
        vm->cashRegister[i].count = DEFAULT_COIN_COUNT;
}

char* calculateChange(VmSystem* vm, int owed){
    struct Changes{
        Denomination denom;
        int count;
    };
    
    int change = owed;
    int iteration = 0;
    char coins[BUFFER_SIZE];
    struct Changes changes[NUM_DENOMS];
    int changesSize = 0;
    char* cache;
    char* toBeReturned = malloc(BUFFER_SIZE);
    memset(changes, 0, sizeof(struct Changes) * NUM_DENOMS); /* Reset everything to 0 */
    
    while(change != 0){
        for(iteration = NUM_DENOMS-1; iteration >= 0; --iteration){
            if( ( change - coinParse(vm->cashRegister[iteration].denom) >= 0) &&
               vm->cashRegister[iteration].count > 1 ){

                changes[changesSize].denom = vm->cashRegister[iteration].denom;
                ++changes[changesSize].count;
                
                change -= coinParse(vm->cashRegister[iteration].denom);
                ++changesSize;
                break;
            }
        }
    }
    
    if(change > 0)
        strcpy(toBeReturned, SYSTEM_COIN_DISPENSE_ERROR);
    else if (changesSize > 0) { /* Do coin transaction (means deduct the coins) */
        for(iteration = 0; iteration < changesSize; ++iteration)
            dispenseCoin(vm, changes[iteration].denom, changes[iteration].count);
        cache = coinParseDecimal(changes[0].denom);
        sprintf(coins, "%s", cache);
        free(cache);
        cache = NULL;
        for(iteration = 1; iteration < changesSize; ++iteration){
            	cache = coinParseDecimal(changes[iteration].denom);
		sprintf(coins, "%s, %s", coins, cache);
		free(cache);
		cache = NULL;
	}
        strcpy(toBeReturned, coins);
    } else
        strcpy(toBeReturned, SYSTEM_COIN_DISPENSE_NO_CHANGES);
    return toBeReturned;
}

void addCoin(VmSystem* vm, Denomination denom, int count){
    int iteration = 0;
    for(iteration = 0; iteration < NUM_DENOMS; ++iteration)
        if(vm->cashRegister[iteration].denom == denom)
            vm->cashRegister[iteration].count = count;
}

void incrementCoin(VmSystem* vm, Denomination denom){
    int iteration = 0;
    for(iteration = 0; iteration < NUM_DENOMS; ++iteration)
        if(vm->cashRegister[iteration].denom == denom)
            vm->cashRegister[iteration].count++;
}

Boolean dispenseCoin(VmSystem* vm, Denomination denom, int count){
    Boolean status = FALSE;
    int iteration = 0;
    for(iteration = 0; iteration < NUM_DENOMS; ++iteration)
        if( (vm->cashRegister[iteration].denom == denom) && (vm->cashRegister[iteration].count > 0) ){
            vm->cashRegister[iteration].count -= count;
            status = TRUE;
        }
    return status;
}

void initCoin(VmSystem* vm){
    int iteration = 0;

    Denomination dictionary[] = {FIVE_CENTS, TEN_CENTS, TWENTY_CENTS, FIFTY_CENTS, ONE_DOLLAR,
        TWO_DOLLARS, FIVE_DOLLARS, TEN_DOLLARS};

    for(iteration = 0; iteration < NUM_DENOMS; ++iteration){
        vm->cashRegister[iteration].denom = dictionary[iteration];
        vm->cashRegister[iteration].count = 0;
    }
}

Denomination toDenom(int coin){
    switch(coin){
        case DECIMAL_DENOM_5CENT:
            return FIVE_CENTS;
        case DECIMAL_DENOM_10CENT:
            return TEN_CENTS;
        case DECIMAL_DENOM_20CENT:
            return TWENTY_CENTS;
        case DECIMAL_DENOM_50CENT:
            return FIFTY_CENTS;
        case DECIMAL_DENOM_1DOLLAR:
            return ONE_DOLLAR;
        case DECIMAL_DENOM_2DOLLAR:
            return TWO_DOLLARS;
        case DECIMAL_DENOM_5DOLLAR:
            return FIVE_DOLLARS;
        case DECIMAL_DENOM_10DOLLAR:
            return TEN_DOLLARS;
        default:
            return FUNCTION_RETURN_ERROR;
    }
    return FUNCTION_RETURN_ERROR;
}
